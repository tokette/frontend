import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-token',
  templateUrl: 'token.html'
})
export class TokenPage {

  constructor(public navCtrl: NavController) {
  }
}
