import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-request',
  templateUrl: 'request.html'
})
export class RequestPage {

  private items: any;
  private displayItems: any;

  constructor(public navCtrl: NavController) {
    this.items = [
      {
        'image': 'assets/imgs/logo.png',
        'name' : 'Franklin',
        'team' : 'Equipe Plouc',
        'date' : '12/10/2018'
      },
      {
        'image': 'assets/imgs/logo.png',
        'name' : 'Edouard',
        'team' : 'Equipe Bruc',
        'date' : '11/10/2018'
      },
      {
        'image': 'assets/imgs/logo.png',
        'name' : 'Bernarlt',
        'team' : 'Equipe Mock',
        'date' : '13/10/2018'
      },
      {
        'image': 'assets/imgs/logo.png',
        'name' : 'Claris',
        'team' : 'Equipe Plouf',
        'date' : '13/10/2018'
      }
    ];
    this.displayItems = this.items;
  }
}
