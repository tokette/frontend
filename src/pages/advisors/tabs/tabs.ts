import { Component } from '@angular/core';

import { TokenPage } from '../token/token';
import { RequestPage } from '../request/request';

@Component({
  templateUrl: 'tabs.html'
})
export class AdvisorsTabsPage {

  tab1Root = TokenPage;
  tab2Root = RequestPage;

  constructor() {

  }
}
