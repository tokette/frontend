import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// import { EmptyPage } from '../users/empty/empty';
// import { UsersTabsPage } from '../users/tabs/tabs';
// import { AdvisorsTabsPage } from '../advisors/tabs/tabs';
import { ChoosePage } from '../actionTeam/choose/choose';
// import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";

import * as _ from 'lodash';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  private apiKey: string;
  private isError: boolean;

  constructor(public navCtrl: NavController/*, private http: HttpClient*/) {

  }

  login() {
    if(_.isNil(this.apiKey)){
      this.isError = true;
      return;
    }
    // const headers = new HttpHeaders({'x-auth-token': this.apiKey, 'Content-Type': 'application/json'});
    // const url = 'http://localhost:8001/api/user'
    // this.http.get(url, {headers: headers})
    //   .subscribe(data => {
    //     localStorage.setItem('user', this.apiKey);
    //     console.log(data)
    //     if(_.includes(data.roles, 'ROLE_USER')){
    //       // if(_.isEmpty(data.teams)){
    //       //   this.navCtrl.push(EmptyPage);
    //       //   return;
    //       // }
    //       if(_.isEmpty(data.teams)){
            this.navCtrl.push(ChoosePage);
      //       return;
      //     }
      //     this.navCtrl.push(UsersTabsPage);
      //   }
      //   if(_.includes(data.roles, 'ROLE_ADVISER')){
      //       this.navCtrl.push(AdvisorsTabsPage);
      //   }
      // }),((err: HttpErrorResponse) =>{
      //   console.log(err)
      // });
  }

}
