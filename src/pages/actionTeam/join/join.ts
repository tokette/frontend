import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { UsersTabsPage } from '../../users/tabs/tabs';

@Component({
  selector: 'page-join',
  templateUrl: 'join.html'
})
export class JoinPage {

  constructor(public navCtrl: NavController) {

  }
  joinTeam(){
    if(true){
      this.navCtrl.push(UsersTabsPage);
    }
  }

}
