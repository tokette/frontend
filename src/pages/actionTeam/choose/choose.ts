import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { CreatePage } from '../create/create';
import { JoinPage } from '../join/join';

@Component({
  selector: 'page-choose',
  templateUrl: 'choose.html'
})
export class ChoosePage {

  constructor(public navCtrl: NavController) {

  }
  goToCreateTeam(){
    this.navCtrl.push(CreatePage);
  }
  goToJoinTeam(){
    this.navCtrl.push(JoinPage);
  }
}
