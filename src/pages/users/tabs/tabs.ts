import { Component } from '@angular/core';

import { GroupPage } from '../group/group';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class UsersTabsPage {

  tab1Root = GroupPage;
  tab2Root = HomePage;
  tab3Root = ContactPage;

  constructor() {

  }
}
