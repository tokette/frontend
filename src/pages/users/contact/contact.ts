import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

import * as _ from 'lodash';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  private items: any;
  private displayItems: any;
  private searchInput: any;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {
    this.items = [
      {
        'image': 'assets/imgs/logo.png',
        'name' : 'Remi D',
        'role' : 'Developpeur React'
      },
      {
        'image': 'assets/imgs/logo.png',
        'name' : 'Philippe K',
        'role' : 'Developpeur Js'
      },
      {
        'image': 'assets/imgs/logo.png',
        'name' : 'Marine B',
        'role' : 'Experte Base de données'
      },
      {
        'image': 'assets/imgs/logo.png',
        'name' : 'Emile Z',
        'role' : 'Architecte'
      }
    ];
    this.displayItems = this.items;
  }

  onInput(e){
    this.displayItems = _.filter(this.items, (obj) => {
      return obj.name.indexOf(this.searchInput) !== -1 || obj.role.indexOf(this.searchInput) !== -1;
    });
  }
  onCancel(e){
    this.displayItems = this.items;
  }
  showConfirm() {
   let confirm = this.alertCtrl.create({
     title: 'Confirmer la demande',
     message: 'Souhaitez vous dépenser 5 token(s) pour ce conseiller?',
     buttons: [
       {
         text: 'Non',
         handler: () => {
           console.log('Disagree clicked');
         }
       },
       {
         text: 'Oui !',
         handler: () => {
           console.log('Agree clicked');
         }
       }
     ]
   });
   confirm.present();
 }

}
