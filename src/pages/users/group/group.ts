import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-group',
  templateUrl: 'group.html'
})
export class GroupPage {

  private items: any;
  private displayItems: any;
  private displayRequest: any;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {
    this.items = [
      {
        'image': 'assets/imgs/logo.png',
        'name' : 'Baptiste G',
        'role' : 'Programmeur'
      },
      {
        'image': 'assets/imgs/logo.png',
        'name' : 'Richard N',
        'role' : 'Designer'
      },
      {
        'image': 'assets/imgs/logo.png',
        'name' : 'Edith M',
        'role' : 'Gaphiste'
      },
      {
        'image': 'assets/imgs/logo.png',
        'name' : 'Annie K',
        'role' : 'Chef d\'équipe'
      }
    ];
    this.displayItems = this.items;
    this.displayRequest = [
      {
        'image': 'assets/imgs/logo.png',
        'name' : 'Nicolas R',
        'date' : '22/10/2018',
        'team' : 'Team'
      },
      {
        'image': 'assets/imgs/logo.png',
        'name' : 'test2',
        'date' : 'test',
        'team' : 'Team'
      },
      {
        'image': 'assets/imgs/logo.png',
        'name' : 'test3',
        'date' : 'test',
        'team' : 'Team'
      },
      {
        'image': 'assets/imgs/logo.png',
        'name' : 'test4',
        'date' : 'test',
        'team' : 'Team'
      }
    ];
  }

  showConfirm() {
   let confirm = this.alertCtrl.create({
     title: 'Confirmer la demande',
     message: 'Souhaitez vous réellement supprimer ce membre?',
     buttons: [
       {
         text: 'Non',
         handler: () => {
           console.log('Disagree clicked');
         }
       },
       {
         text: 'Oui !',
         handler: () => {
           console.log('Agree clicked');
         }
       }
     ]
   });
   confirm.present();
 }

}
