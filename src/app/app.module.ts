import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginPage } from '../pages/login/login';
import { GroupPage } from '../pages/users/group/group';
import { ContactPage } from '../pages/users/contact/contact';
import { HomePage } from '../pages/users/home/home';
import { UsersTabsPage } from '../pages/users/tabs/tabs';

import { ChoosePage } from '../pages/actionTeam/choose/choose';
import { CreatePage } from '../pages/actionTeam/create/create';
import { JoinPage } from '../pages/actionTeam/join/join';

import { AdvisorsTabsPage } from '../pages/advisors/tabs/tabs';
import { RequestPage } from '../pages/advisors/request/request';
import { TokenPage } from '../pages/advisors/token/token';

import { EmptyPage } from '../pages/users/empty/empty';

import { RoundProgressModule } from 'angular-svg-round-progressbar';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';



@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    GroupPage,
    ContactPage,
    HomePage,
    UsersTabsPage,
    EmptyPage,
    ChoosePage,
    CreatePage,
    JoinPage,
    AdvisorsTabsPage,
    RequestPage,
    TokenPage
  ],
  imports: [
    BrowserModule,
    RoundProgressModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    GroupPage,
    ContactPage,
    HomePage,
    UsersTabsPage,
    EmptyPage,
    ChoosePage,
    CreatePage,
    JoinPage,
    AdvisorsTabsPage,
    RequestPage,
    TokenPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
